module.factory('PhoneNumberEdit', function($http, $timeout, $location, $window) {
	return {
		fromPage : null,
		toPage : 'templates/profile/profileEdit.html',
		menuSwipeable : false,
		country : null,
		phoneNumber : null
	};
});