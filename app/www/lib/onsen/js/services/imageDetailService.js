module.factory('ImageDetail', function($http, $timeout, $location, $window, $rootScope) {
	var self = {
		data : null,
		title : null,
		showOptionsBar : false,
		showSelectBar : false,
		hideDelete : false,
		resetBar : function(){
			self.showOptionsBar = false;
			self.showSelectBar = false;
			self.hideDelete = false;
		},
		selectClicked : function(){}
	};
	return self;
});