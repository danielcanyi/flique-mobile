module.factory('User', function($http, Url, Country, $rootScope) {
	var self = {
		id : null,
		facebook_id : null,
		name : null,
		email : null,
		status : null,
		country : null,
		gender : null,
		marital : null,
		interests : null,
		dream_bucket : null,
		dob : null,
		phoneNumberCountry : null,
		phoneNumber : null,
		profileImage : null,
		coverImage : null,
		editing : false,
		profileCreateMode : false,
		cached : false,
		editData : {
			name : null,
			email : null,
			status : null,
			country : null,
			dob : null,
			phoneNumberCountry : null,
			gender : null,
			marital : null,
			interests : null,
			dream_bucket : null,
			phoneNumber : null,
			profileImage : null,
			coverImage : null,
			selectionData : null
		},
		cache : function(data)
		{
			self.id = data.user_id;
    		localStorage.setItem('userId', self.id);
			self.name = data.name;
			for(var i = 0; i < Country.countries.length; i++)
			{
				if (Country.countries[i].alpha2 == data.user_country)
				{
					self.country = Country.countries[i];
				}
				if (Country.countries[i].alpha2 == data.user_phone_country)
				{
					self.phoneNumberCountry = Country.countries[i];
				}
			}
			self.status = data.profile_status;
			self.phoneNumber = data.user_phone;
			self.profileImage = data.profile_image;
			self.coverImage = data.cover_image;
			self.dob = data.dob;
			self.facebook_id = data.facebook_id;
			self.email = data.user_email;
			self.gender = data.gender;
			self.marital = data.marital;
			self.interests = data.interests;
			self.dream_bucket = data.dream_bucket;
			
            $rootScope.Notification.gcmSent = false;
            $rootScope.Notification.sendGcmId();
            self.cached = true;
		},
		get : function(userId)
		{
			return $http({
	            method: 'GET',
	            url: Url.api + '/user/' + userId + '/details'});
		},
		update : function(updateObj){
			var copyFields = ['name'
				, 'status'
				, 'country'
				, 'dob'
				, 'phoneNumber'
				, 'phoneNumberCountry'
				, 'profileImage'
				, 'coverImage'
				, 'gender'
				, 'marital'
				, 'interests'
				, 'dream_bucket'
				];
			var obj = {};
			for(var i = 0; i < copyFields.length; i++)
			{
				obj[copyFields[i]] = self[copyFields[i]];
			}
			for (var key in updateObj) {
			  if (updateObj.hasOwnProperty(key)) {
			  	obj[key] = updateObj[key];
			  }
			}

			return $http({
	            method: 'POST',
	            url: Url.api + '/user/' + self.id + '/profile/update',
	            data: {
	                profile_status : obj.status,
	                full_name : obj.name,
	                phone : obj.phoneNumber,
	                gender : obj.gender,
	                marital : obj.marital,
	                interests : obj.interests,
	                dream_bucket : obj.dream_bucket,
	                phone_country : (obj.phoneNumberCountry ? obj.phoneNumberCountry.alpha2 : null),
	                country : (obj.country ? obj.country.alpha2 : null),
	                profile_image : obj.profileImage,
	                cover_image : obj.coverImage,
	                dob : obj.dob
	            },                    
	        });
		}
	};
	return self;
});