module.factory('MediaLibrary', function($http, $timeout, $location, $window) {
	var self = {
		data :[],
		sortedData : [],
		deleteFileIds : [],
		deleteFiles : function(){

			var newData = [];
			for(var i = 0; i < self.data.length; i++)
			{
				var willDelete = false;
				for(var j = 0; j < self.deleteFileIds.length; j++)
				{
					if (self.deleteFileIds[j] == self.data[i].id)
					{
						willDelete = true;
						break;
					}
				}
				if ( ! willDelete) newData.push(self.data[i]);
			}
			self.data = newData;
			self.sort();
		},
		title : null,
		selected : [],
		selectionOk : false,
		sort: function(){
			self.sortedData = [];
			var currMonth = null;
			var currYear = null;
			var currMonthData = {};

			var indexInCurrMonth = 0; // gonna split data into groups of 3
			//data must be sorted by date
			for(var i = 0; i < self.data.length; i++)
			{
				var date = new Date(self.data[i].exchanged_at);
				var month = date.getMonth();
				var year = date.getFullYear();
				if (currMonth == null)
				{
					currMonth = month;
					currYear = year;
					currMonthData = {
						date : ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][month] + " " + year,
						data : [],
						count : 0
					};
				}
				else if (currMonth != month || currYear != year)
				{
					self.sortedData.push(currMonthData);
					indexInCurrMonth = 0;
					currMonth = month;
					currYear = year;
					currMonthData = {
						date : ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][month] + " " + year,
						data : [],
						count : 0
					};
				}
				currMonthData.count++;
				if ( ! currMonthData.data[$window.Math.floor(indexInCurrMonth/3)]) currMonthData.data[$window.Math.floor(indexInCurrMonth/3)] = [];
				currMonthData.data[$window.Math.floor(indexInCurrMonth/3)].push(self.data[i]);
				indexInCurrMonth++;
			}
			//add the last month
			self.sortedData.push(currMonthData);
		},
		reset : function(){ 
			self.selected = []; 
			self.selectionOk = false;}
	};
	return self;
});