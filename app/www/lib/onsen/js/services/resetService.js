module.factory('Reset', function(ImageSelection, ImageUpload) {
	return {
		reset : function(){
			ImageSelection.reset();
			ImageUpload.reset();
		}
	};
});