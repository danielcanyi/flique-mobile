module.factory('Navigation', function($http, $rootScope, $timeout, $location, $window, Chat) {
	var self = {
		backStack : []
		, currPage : null
		, goToCurrPage : function(){
			$rootScope.menu.setMainPage(self.currPage, {closeMenu : true});
		}
		, goToPage : function(page, clearBackStack)
		{
			Chat.currId = null; //stop chat loop
			clearBackStack = (typeof clearBackStack !== 'undefined') ? clearBackStack : false;
			if (clearBackStack) self.backStack = [];
			else if (self.currPage) self.backStack.push(self.currPage);
			self.currPage = page;
			self.goToCurrPage();
		}
		, popBackStack : function()
		{
			var currStack = self.backStack;
			currStack.pop();
			self.backStack = currStack;
		}
		, goBack : function()
		{
			Chat.currId = null; //stop chat loop
			var currStack = self.backStack;
			self.currPage = currStack.pop();
			self.backStack = currStack;
			self.goToCurrPage();
		}
	};
	return self;
});