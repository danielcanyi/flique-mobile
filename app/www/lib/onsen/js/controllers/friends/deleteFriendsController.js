module.controller('DeleteFriendsController', function($scope, $rootScope, $window, Friend, $http, Url, User, $timeout){
  $rootScope.menuSwipeable = true
  $scope.Friend = Friend;
  $scope.search = "";

  $scope.$watch('search', function(){
  	Friend.friendsSorted = Friend.applySearch(Friend.friendsSorted, $scope.search);
  });

  Friend.selectedIds = [];
  Friend.selectReload();

  $scope.nextClicked = function()
  {
    ons.notification.confirm({
      message: 'Are you sure you want to remove those friends from your Friend list?',
      // or messageHTML: '<div>Message in HTML</div>',
      title: 'Confirmation',
      buttonLabels: ['Cancel', 'Ok'],
      animation: 'default', // or 'none'
      primaryButtonIndex: 1,
      cancelable: true,
      callback: function(index) {
        if (index == 0)
        {
          //cancel
        }
        else if (index == 1)
        {
          //ok
          spinnerplugin.show();
          $http({
                  method: 'POST',
                  url: Url.api + '/user/' + User.id + '/delete-friends',
                  data: {
                    user_ids : Friend.selectedIds
                  },                    
              }).success(function(data){
                spinnerplugin.hide();
                $rootScope.Navigation.goBack();
              });
        }
      }
    });
  }

});