module.controller('FriendsController', function($scope, $rootScope, $window, $document, Friend, $http, Url, User, $timeout){
  $rootScope.menuSwipeable = true
  $scope.Friend = Friend;
  $scope.search = "";

  $scope.openMenuStr = null;

  $scope.swipeLeft = function(_char, _index)
  {
    $scope.openMenuStr = _char + "_" + _index;
  }

  $scope.$watch('search', function(){
  	Friend.friendsSorted = Friend.applySearch(Friend.friendsSorted, $scope.search);
  });


  $http({
    method: 'GET',
    url: Url.api + '/user/' + User.id + '/get-all-friends'})
  .success(function(data){
  	data = data.results[0];
  	if (data.status == 'SUCCESS')
  	{
  		Friend.friends = [];
  		for(var i = 0; i < data.friends.length; i++)
  		{
  			Friend.friends.push({
  				id : data.friends[i].friend_user_id
  				, name : data.friends[i].friend_name
  				, status : data.friends[i].friend_profile_status
  				, photos : [data.friends[i].friend_profile_image]
          , muted : data.friends[i].muted
  			});
  		}
		Friend.friendsSorted = Friend.sort(Friend.friends);
  	}
  });

  var menuButtonJustClicked = false;
  $scope.friendClicked = function(_char, _index)
  {
    if (menuButtonJustClicked)
    {
      menuButtonJustClicked = false;
      return;
    }

    if ($scope.openMenuStr == _char + "_" + _index){
      $scope.openMenuStr = null;
      return;
    } 
    Friend.currFriendId = Friend.friendsSorted[_char][_index].id;
    $rootScope.Navigation.goToPage('templates/friends/friendProfile.html');
  }

  $scope.muteClicked = function(char, index)
  {
    menuButtonJustClicked = true;
    if (Friend.friendsSorted[char][index].muted == 0)
    {
      $scope.currMuteFriendChar = char;
      $scope.currMuteFriendIndex = index;
      $rootScope.mutePopover.show('#mute_' + char + '_' + index);
      return;
    }
    
    spinnerplugin.show();
    //close friend menu
    $scope.openMenuStr = null;
    $http({
            method: 'POST',
            url: Url.api + '/user/' + User.id + '/friend/unmute/' + Friend.friendsSorted[char][index].id,
        }).success(function(data){
          spinnerplugin.hide();
          Friend.friendsSorted[char][index].muted = 0;
        });    
  }

  $rootScope.mute = function(days)
  {
    spinnerplugin.show();
    $rootScope.mutePopover.hide();

    //close friend menu
    $scope.openMenuStr = null;
    $http({
            method: 'POST',
            url: Url.api + '/user/' + User.id + '/friend/mute/' + Friend.friendsSorted[$scope.currMuteFriendChar][$scope.currMuteFriendIndex].id,
            data : {days : days}
        }).success(function(data){
          spinnerplugin.hide();
          Friend.friendsSorted[$scope.currMuteFriendChar][$scope.currMuteFriendIndex].muted = 1;
        });
  }

  $scope.deleteClicked = function()
  {
    $rootScope.Navigation.goToPage('templates/friends/delete.html');
  }
});