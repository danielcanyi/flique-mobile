module.controller('AddFriendsController', function($scope, $rootScope, $window, Friend, $http, Url, User){
  $rootScope.menuSwipeable = true
  $scope.Friend = Friend;
  $scope.search = "";

  $scope.$watch('search', function(){
    Friend.requestsSorted = Friend.applySearch(Friend.requestsSorted, $scope.search);
  	Friend.requestsSentSorted = Friend.applySearch(Friend.requestsSentSorted, $scope.search);
    Friend.suggestionsSorted = Friend.applySearch(Friend.suggestionsSorted, $scope.search);
  });

  var reload = function()
  {
    $http({
      method: 'GET',
      url: Url.api + '/user/' + User.id + '/get-friend-requests'})
    .success(function(data){
      data = data.results[0];
      if (data.status == 'SUCCESS')
      {
        Friend.requests = [];
        $scope.hasRequests = false;
        for(var i = 0; i < data.requests.length; i++)
        {
          Friend.requests.push({
            id : data.requests[i].user_id
            , name : data.requests[i].name
            , status : data.requests[i].profile_status
            , photos : [data.requests[i].profile_image]
          });
          $scope.hasRequests = true;
        }
      Friend.requestsSorted = Friend.sort(Friend.requests);
      }
    });
    $http({
      method: 'GET',
      url: Url.api + '/user/' + User.id + '/get-sent-requests'})
    .success(function(data){
      data = data.results[0];
      if (data.status == 'SUCCESS')
      {
        Friend.requestsSent = [];
        $scope.hasRequestsSent = false;
        for(var i = 0; i < data.requests.length; i++)
        {
          $scope.hasRequestsSent = true;
          Friend.requestsSent.push({
            id : data.requests[i].friend_user_id
            , name : data.requests[i].friend_name
            , status : data.requests[i].friend_profile_status
            , photos : [data.requests[i].friend_profile_image]
          });
        }
      Friend.requestsSentSorted = Friend.sort(Friend.requestsSent);
      }
    });
    $http({
      method: 'GET',
      url: Url.api + '/user/' + User.id + '/get-friend-suggestions'})
    .success(function(data){
      data = data.results[0];
      if (data.status == 'SUCCESS')
      {
        Friend.suggestions = [];
        $scope.hasSuggestions = false;
        for(var i = 0; i < data.suggestions.length; i++)
        {
          $scope.hasSuggestions = true;
          Friend.suggestions.push({
            id : data.suggestions[i].friend_user_id
            , name : data.suggestions[i].friend_name
            , status : data.suggestions[i].friend_profile_status
            , photos : [data.suggestions[i].friend_profile_image]
          });
        }
      Friend.suggestionsSorted = Friend.sort(Friend.suggestions);
      }
    });
  };
  reload();

  $scope.confirmRequest = function(friendUserId)
  {
    spinnerplugin.show();
    $http({
      method: 'POST',
      url: Url.api + '/user/' + User.id + '/friend/add/' + friendUserId})
    .success(function(data){
      spinnerplugin.hide();
      reload();
    });
  }

  $scope.sendRequest = function(friendUserId)
  {
    spinnerplugin.show();
    $http({
      method: 'POST',
      url: Url.api + '/user/' + User.id + '/friend/send-request/' + friendUserId})
    .success(function(data){
      spinnerplugin.hide();
      reload();
    });
  }

  $scope.cancelRequest = function(friendUserId)
  {
    spinnerplugin.show();
    $http({
      method: 'POST',
      url: Url.api + '/user/' + User.id + '/friend/cancel-request/' + friendUserId})
    .success(function(data){
      spinnerplugin.hide();
      reload();
    });
  }

  $scope.rejectRequest = function(friendUserId)
  {
    spinnerplugin.show();
    $http({
      method: 'POST',
      url: Url.api + '/user/' + User.id + '/friend/reject-request/' + friendUserId})
    .success(function(data){
      spinnerplugin.hide();
      reload();
    });
  }

});