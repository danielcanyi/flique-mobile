module.controller('InviteController', function($scope, $rootScope, $window, Friend){
 	$rootScope.menuSwipeable = true
 	$scope.Friend = Friend;
	Friend.contactsSorted = Friend.sort(Friend.contacts);
 	$scope.search = "";
 	$scope.allSelected = false;

	$scope.$watch('search', function(){
		Friend.contactsSorted = Friend.applySearch(Friend.contactsSorted, $scope.search);
	});

	$scope.allClicked = function(){
		Friend.contactsSorted = Friend.applyToAll(Friend.contactsSorted, function(element){
			if ($scope.allSelected) element.selected = false;
			else element.selected = true;
			return element;
		});
		$scope.allSelected = ! $scope.allSelected;
	};

	$scope.contactClicked = function(char, index)
	{
		Friend.contactsSorted[char][index].selected = ! Friend.contactsSorted[char][index].selected;
		if (Friend.contactsSorted[char][index].selected)
		{
			$scope.allSelected = true;
			Friend.contactsSorted = Friend.applyToAll(Friend.contactsSorted, 
				function(element){
					if ( ! element.selected) $scope.allSelected = false;
					return element;
			});
		}
		else $scope.allSelected = false;
	}
});