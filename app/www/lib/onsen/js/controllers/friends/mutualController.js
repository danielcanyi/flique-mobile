module.controller('MutualController', function($scope, $rootScope, $window, Friend, $http, Url, User){
  $rootScope.menuSwipeable = true
  $scope.Friend = Friend;
  $scope.search = "";

  $scope.$watch('search', function(){
  	Friend.currMutualFriendsSorted = Friend.applySearch(Friend.currMutualFriendsSorted, $scope.search);
  });
});