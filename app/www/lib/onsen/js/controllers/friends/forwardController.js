module.controller('ForwardController', function($scope, $rootScope, $window, Friend, $http, Url, User, $timeout){
  $rootScope.menuSwipeable = true
  $scope.Friend = Friend;
  $scope.search = "";

  $scope.$watch('search', function(){
  	Friend.friendsSorted = Friend.applySearch(Friend.friendsSorted, $scope.search);
  });

  var reload = function(){
    spinnerplugin.show();
    $http({
      method: 'GET',
      url: Url.api + '/user/' + User.id + '/get-all-friends'})
    .success(function(data){
      data = data.results[0];
      if (data.status == 'SUCCESS')
      {
        Friend.friends = [];
        for(var i = 0; i < data.friends.length; i++)
        {
          Friend.friends.push({
            id : data.friends[i].friend_user_id
            , name : data.friends[i].friend_name
            , status : data.friends[i].friend_profile_status
            , photos : [data.friends[i].friend_profile_image]
            , muted : data.friends[i].muted
          });
        }
      Friend.friendsSorted = Friend.sort(Friend.friends);
      }
      spinnerplugin.hide();
    });
    $scope.selectedFriends = {};
    
  }

  reload();

  $scope.friendClicked = function(char, index)
  {
    if ( ! $scope.selectedFriends[char]) $scope.selectedFriends[char] = [];
    if ($scope.selectedFriends[char][index]) $scope.selectedFriends[char][index] = false;
    else $scope.selectedFriends[char][index] = true;
  }

  var carouselIndexes = {};


  $scope.forwardClicked = function()
  {
      spinnerplugin.show();
      var forwardIds = [];
      for(var key in $scope.selectedFriends)
      {
        if ( ! $scope.selectedFriends.hasOwnProperty(key)) continue;
        for(var i = 0; i < $scope.selectedFriends[key].length; i++)
        {
          if ($scope.selectedFriends[key][i]) forwardIds.push(Friend.friendsSorted[key][i].id);
        }
      }
      if (forwardIds.length < 1) return;

      $http({
          method: 'POST',
          url: Url.api + '/user/' + User.id + '/forward-media',
          data: {
            user_ids : forwardIds,
            file_ids : Friend.forwardFileIds
          },                    
      }).success(function(data){
        spinnerplugin.hide();
        alert('Forwarded!');
        $rootScope.Navigation.goBack();
      });
  }

});