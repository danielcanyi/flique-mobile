module.controller('ProfileDobEditController', function($scope, $rootScope, $window, User){
  $rootScope.menuSwipeable = true;
  var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var yyyy = today.getFullYear();

  var padDateVal = function(num)
  {
  	return ((num < 10) ? "0" : "") + num;
  };
  $scope.dateDays = [];
  for(var i = 1; i < 32; i++)
  {
  	$scope.dateDays.push({
  		name : i,
  		val : padDateVal(i)
  	});
  }

  var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  $scope.dateMonths = [];
  for(var i = 0; i < months.length; i ++)
  {
  	$scope.dateMonths.push({
  		name : months[i],
  		val : padDateVal(i+1)
  	})
  }

  $scope.dateYears = [];
  for(var i = 0; i < 100; i++)
  {
  	$scope.dateYears.push({
  		name : yyyy - i,
  		val : yyyy - i
  	});
  }


  $scope.dateSelectedDay = $scope.dateDays[dd-1];
  $scope.dateSelectedMonth = $scope.dateMonths[mm-1];
  var selectedYear = yyyy;
  for(var i = 0; i < $scope.dateYears.length; i++)
  {
  	if ($scope.dateYears[i].val == yyyy) $scope.dateSelectedYear = $scope.dateYears[i];
  }

    $scope.dateOkClicked = function(){

    	var e = document.getElementById("dateSelectedDay");
  		var val = e.options[e.selectedIndex].value;
  		$scope.dateSelectedDay = $scope.dateDays[val];
    	var e = document.getElementById("dateSelectedMonth");
  		var val = e.options[e.selectedIndex].value;
  		$scope.dateSelectedMonth = $scope.dateMonths[val];
    	var e = document.getElementById("dateSelectedYear");
  		var val = e.options[e.selectedIndex].value;
  		$scope.dateSelectedYear = $scope.dateYears[val];

    	var dob = $scope.dateSelectedYear.val + "-" + padDateVal($scope.dateSelectedMonth.val) + "-" + padDateVal($scope.dateSelectedDay.val);
      User.editData.dob = dob;
      $rootScope.Navigation.goBack();
    }
});