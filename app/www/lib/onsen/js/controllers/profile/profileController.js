module.controller('ProfileController', function($scope, $rootScope, $window, PhoneNumberEdit, Chat, User, $timeout, ImageSelection, ImageUpload, Reset, Friend, $http, Url, MediaLibrary){
        $rootScope.menuSwipeable = true;
        $scope.profileShowLocation = true;

        $scope.groupChatsCreatedCount = 0;
        $scope.mediaSentCount = 0;
        $scope.mediaReceivedCount = 0;

        $scope.profileShowChat = true;

        Reset.reset();

        var refresh = function(){
            if (User.phoneNumber)
            {
                $scope.profileShowContact = true;
                $scope.profilePhoneStr = "";
                if (User.phoneNumberCountry && User.phoneNumberCountry != undefined) $scope.profilePhoneStr = "(+" + User.phoneNumberCountry.code + ") ";
                $scope.profilePhoneStr += User.phoneNumber;
            }
            else $scope.profileShowContact = false;

            if (User.country) $scope.profileCountryStr = User.country.name;
            else $scope.profileShowLocation = false;

            if (User.name) $scope.profileDisplayUsernameStr = $scope.profileUsernameStr = User.name;

            if (User.status) $scope.profileDisplayStatusStr = $scope.profileStatusStr = User.status;

            $scope.profileImage = User.profileImage;
            $scope.coverImage = User.coverImage;

            if (User.dob)
            {
                var dob = new Date(User.dob);
                var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                $scope.profileDobStr = dob.getDate() + " " + months[dob.getMonth()] + " " + dob.getFullYear();

                $scope.profileBirthdayStr = dob.getDate() + " " + months[dob.getMonth()];

                $scope.age = $window.Math.floor(((new Date()) - dob)/(1000*60*60*24*365));
            }

            $scope.parsedInterests = User.interests ? JSON.parse(User.interests) : null;
            $scope.parsedDreamBucket = User.dream_bucket ? JSON.parse(User.dream_bucket) : null;
        };

        refresh();

        User.get(User.id).success(function(data){
            User.cache(data.results[0].data);
            refresh();

            $http({
                method: 'GET',
                url: Url.api + '/user/' + User.id + '/get-sent-media-count'})
                .success(function(data){
                    $scope.mediaSentCount = data.results.count;
                });  

            $http({
                method: 'GET',
                url: Url.api + '/user/' + User.id + '/get-received-media-count'})
                .success(function(data){
                    $scope.mediaReceivedCount = data.results.count;
                });  

            $http({
                method: 'GET',
                url: Url.api + '/user/' + User.id + '/chat/created-count'})
                .success(function(data){
                    $scope.groupChatsCreatedCount = data.results.count;
                });  
        });

        $scope.editClicked = function(){
            $rootScope.Navigation.goToPage('templates/profile/profileEdit.html');
        };

        $scope.groupChatClicked = function(){
            Chat.groupsSelected = true;
            Chat.loadUserCreatedChats = true;
            $rootScope.Navigation.goToPage('templates/chat/chatList.html');
        };

        $scope.mediaSentClicked = function(){
            spinnerplugin.show();
            $http({
                method: 'GET',
                url: Url.api + '/user/' + User.id + '/get-sent-media'})
                .success(function(data){
                    spinnerplugin.hide();
                    MediaLibrary.data = data.results ? data.results : [];
                    MediaLibrary.title = "Media sent by me";
                    $rootScope.Navigation.goToPage('templates/misc/mediaLibrary.html');
                });
        };

        $scope.mediaReceivedClicked = function(){
            spinnerplugin.show();
            $http({
                method: 'GET',
                url: Url.api + '/user/' + User.id + '/get-received-media'})
                .success(function(data){
                    spinnerplugin.hide();
                    MediaLibrary.data = data.results ? data.results : [];
                    MediaLibrary.title = "Media i received";
                    $rootScope.Navigation.goToPage('templates/misc/mediaLibrary.html');
                });
        };

});