module.controller('SignupController', function($scope, $rootScope, $window, User, $http, Url){
  $rootScope.menuSwipeable = false;
    $scope.signupClicked = function(){
    	if ( ! $scope.email)
    	{
    		alert('Please key in an email address');
    		return
    	}
    	if ( ! $scope.password)
    	{
    		alert('Please key in a password');
    		return;
    	}
    	if ($scope.password != $scope.password_confirm)
    	{
    		alert('The passwords do not match');
    		return;
    	}

    	spinnerplugin.show();
    	$http({
	            method: 'POST',
	            url: Url.api + '/register',
	            data: {
	            	username : $scope.email,
	            	password : $scope.password
	            }                    
	        }).success(function(data){
		    	spinnerplugin.hide();
	        	data = data.results[0];
	        	if (data.status == 'SUCCESS')
	        	{
                    User.cache(data);
			    	$rootScope.Navigation.goToPage('templates/login/emailVerification.html');
	        	}
	        	else alert('Sorry, there has been an error');
	        })
	        .error(function(){
		    	spinnerplugin.hide();
	        	alert('Error');
	        });
    }

    $scope.signinClicked = function(){
        $rootScope.Navigation.goBack();
    }
});