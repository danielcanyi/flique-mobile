module.controller('EmailVerificationController', function($scope, $rootScope, $window, PhoneNumberEdit, $http, Url, User){
  $rootScope.menuSwipeable = false;
    $scope.confirmClicked = function(){

    	spinnerplugin.show();
    	$http({
	            method: 'POST',
	            url: Url.api + '/register/activation',
	            data: {
	            	activation_code : $scope.code
	            }                    
	        }).success(function(data){
		    	spinnerplugin.hide();
	        	data = data.results[0];
	        	if (data.status == 'SUCCESS')
	        	{
	        		User.id = data.user_id;
					PhoneNumberEdit.menuSwipeable = false;
				    PhoneNumberEdit.phoneNumber = null;
				    PhoneNumberEdit.country = null;
					$rootScope.Navigation.goToPage('templates/misc/phoneNumber.html');
	        	}
	        	else alert(data.message.error);
	        })
	        .error(function(){
		    	spinnerplugin.hide();
	        	alert('Error');
	        });

    }
});