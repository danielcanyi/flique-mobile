module.controller('LoginController', function($scope, $rootScope, $window, Reset, Fb, User, $http, Url, PhoneNumberEdit){
  $rootScope.menuSwipeable = false;
	Reset.reset();

	$scope.email = 'asd@asd.com';
	$scope.password = 'asd';
	
	var goToPhone = function(){
		PhoneNumberEdit.menuSwipeable = false;
	    PhoneNumberEdit.phoneNumber = null;
	    PhoneNumberEdit.country = null;
		$rootScope.Navigation.goToPage('templates/misc/phoneNumber.html');
	};

	var proceedToUserArea = function()
	{
    	$rootScope.Navigation.goToPage('templates/chat/chatList.html');
	}

    $scope.fbClicked = function(){
    	Fb.loginSuccess = function(userData){
	    	spinnerplugin.show();
	    	$http({
		            method: 'POST',
		            url: Url.api + '/sign-in/fb-sign-in',
		            data: {
		            	fb_user_email : userData.email,
		            	fb_user_id : userData.id,
		            	name : userData.name
		            }                    
		        }).success(function(data){
			    	spinnerplugin.hide();
		        	data = data.results[0];
		        	if (data.status == 'SUCCESS')
		        	{
		        		User.cache(data.message[0]);
		        		if (User.phoneNumber)
		        		{
		        			localStorage.setItem('facebook_id', User.facebook_id);
		        			proceedToUserArea();
		        		}
					    else
					    	goToPhone();
		        	}
		        	else alert('Invalid Login')
		        })
		        .error(function(){
			    	spinnerplugin.hide();
		        	alert('Error');
		        });
    	};
    	Fb.loginFailure = function(error)
    	{
    		alert("" + error);
			localStorage.removeItem('facebook_id');
    	}
    	Fb.login();
    }

    $scope.loginClicked = function(){
    	User.profileCreateMode = false;
    	spinnerplugin.show();
    	$http({
            method: 'POST',
            url: Url.api + '/sign-in',
            data: {
            	username : $scope.email,
            	password : $scope.password
            }                    
        }).success(function(data){
	    	spinnerplugin.hide();
        	data = data.results[0];
        	if (data.status == 'SUCCESS')
        	{
        		User.cache(data.message[0]);
        		if ( ! data.message[0].activated_at)
			    	$rootScope.Navigation.goToPage('templates/login/emailVerification.html');
        		else if ( ! User.phoneNumber) goToPhone();
		    	else
		    	{
		    		try{
	        			localStorage.setItem('email', $scope.email);
	        			localStorage.setItem('password', $scope.password);
		    		}
		    		catch(err)
		    		{
		    			alert(err.message);
		    		}

        			proceedToUserArea();
		    	}
        	}
        	else
        	{
        		alert('Invalid Login');
				localStorage.removeItem('email');
				localStorage.removeItem('password');
        	}
        })
        .error(function(){
	    	spinnerplugin.hide();
        	alert('Error');
        });
    }

    $scope.signupClicked = function(){
    	User.profileCreateMode = true;
    	$rootScope.Navigation.goToPage('templates/login/signup.html');
    }

	  document.addEventListener('deviceready', function() {
		if ($rootScope.firstLoad) $rootScope.firstLoad = false;
		else
		{
			localStorage.removeItem('email');
			localStorage.removeItem('password');
			localStorage.removeItem('facebook_id');
		}
		    //localStorage.removeItem('facebook_id');
		    if (localStorage.getItem('email') && localStorage.getItem('password'))
		    {
				$scope.email = localStorage.getItem('email');
				$scope.password = localStorage.getItem('password');
				$scope.loginClicked();
		    }
		    else if (localStorage.getItem('facebook_id'))
		    {
		    	try
		    	{
			    	$scope.fbClicked();

		    	}
		    	catch(error)
		    	{
		    		alert(error);
		    		localStorage.removeItem('facebook_id');
		    	}
		    }
		});

});