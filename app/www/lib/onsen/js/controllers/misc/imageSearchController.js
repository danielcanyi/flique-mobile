module.controller('ImageSearchController', function($scope, $rootScope, $window, ImageSearch, $timeout, $http, ImageDetail){
	$scope.ImageSearch = ImageSearch;
	$scope.imageSize = (window.innerWidth-40)/3 - 5;
	$scope.search = "";

	var sortData = function()
	{
		ImageSearch.sortedData = [];
		for(var i = 0; i < ImageSearch.data.length; i++)
		{
			if ( ! ImageSearch.sortedData[$window.Math.floor(i/3)]) ImageSearch.sortedData[$window.Math.floor(i/3)] = [];
			ImageSearch.sortedData[$window.Math.floor(i/3)].push(ImageSearch.data[i]);
		}
		console.log(ImageSearch.sortedData);
	}

	var runSearch = function()
	{
		if ($scope.search.length < 1)
		{
			sortData();
			return;
		}
		$scope.searching = true;
	    $http({
	        method: 'GET',
	        url: 'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=fd42e68aac5b82c1949ebb705905d2ce&text=' + $scope.search +'&per_page=40&format=json&nojsoncallback=1'})
	        .success(function(data){
				$scope.searching = false;
				ImageSearch.data = [];
				for(var i = 0; i < data.photos.photo.length; i++)
				{
					ImageSearch.data.push({
						medium : 'https://farm' 
							+ data.photos.photo[i].farm 
							+ '.staticflickr.com/' 
							+ data.photos.photo[i].server 
							+ '/' 
							+ data.photos.photo[i].id 
							+ '_'
							+ data.photos.photo[i].secret
							+ '_c.jpg',
						square : 'https://farm' 
							+ data.photos.photo[i].farm 
							+ '.staticflickr.com/' 
							+ data.photos.photo[i].server 
							+ '/' 
							+ data.photos.photo[i].id 
							+ '_'
							+ data.photos.photo[i].secret
							+ '.jpg'
					});
				}
				sortData();
	        });
	}

	var searchTimeout = null;
	$scope.$watch('search', function(newValue, oldValue)
	{
	    $timeout.cancel(searchTimeout);
	    searchTimeout = $timeout(function(){
	    	runSearch();
	    }, 1000);
	});

	$scope.imageClicked = function(src)
	{
		ImageDetail.data = {file_location : src};
		ImageDetail.title = "Web Image";
		ImageDetail.resetBar();
		ImageDetail.showSelectBar = true;
		$rootScope.Navigation.goToPage('templates/misc/imageDetail.html');
	}
});