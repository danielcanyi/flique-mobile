module.controller('MediaLibraryController', function($scope, $rootScope, $window, MediaLibrary, ImageDetail, Friend, $http, Url, User){
	$scope.MediaLibrary = MediaLibrary;
	
	MediaLibrary.sort();
	$scope.imageSize = (window.innerWidth-40)/3 - 5;

	$scope.selectClicked = function()
	{
		$scope.selectMode = true;
	}

	var _handleBottomBar = function()
	{
		$rootScope.showBottomBar = false;
		for(var i = 0; i < MediaLibrary.sortedData.length; i++)
		{
			for(var j = 0; j < MediaLibrary.sortedData[i].data.length; j++)
			{
				for(var k = 0; k < MediaLibrary.sortedData[i].data[j].length; k++)
				{
					if (MediaLibrary.sortedData[i].data[j][k].selected)
					{
						$rootScope.showBottomBar = true;
						return;
					}
				}
			}
		}
	}

	$scope.selectCancelClicked = function()
	{
		for(var i = 0; i < MediaLibrary.sortedData.length; i++)
		{
			for(var j = 0; j < MediaLibrary.sortedData[i].data.length; j++)
			{
				for(var k = 0; k < MediaLibrary.sortedData[i].data[j].length; k++)
				{
					MediaLibrary.sortedData[i].data[j][k].selected =  false;
				}
			}
		}
		_handleBottomBar();
		$scope.selectMode = false;
	}

	$scope.imageClicked = function(exchange_id)
	{
		for(var i = 0; i < MediaLibrary.sortedData.length; i++)
		{
			for(var j = 0; j < MediaLibrary.sortedData[i].data.length; j++)
			{
				for(var k = 0; k < MediaLibrary.sortedData[i].data[j].length; k++)
				{
					if (MediaLibrary.sortedData[i].data[j][k].exchange_id != exchange_id) continue;
					if ( ! $scope.selectMode)
					{
						ImageDetail.data = MediaLibrary.sortedData[i].data[j][k];
						ImageDetail.title = "Image " + MediaLibrary.sortedData[i].data[j][k].id;
						ImageDetail.resetBar();
						ImageDetail.showOptionsBar = true;
						$rootScope.Navigation.goToPage('templates/misc/imageDetail.html');
						return;
					}
					MediaLibrary.sortedData[i].data[j][k].selected =  ! MediaLibrary.sortedData[i].data[j][k].selected;
				}
			}
		}
		_handleBottomBar();
	}

	$rootScope.bottomBarDeleteClicked = function()
	{
	    ons.notification.confirm({
	      message: 'Are you sure you want to delete?',
	      // or messageHTML: '<div>Message in HTML</div>',
	      title: 'Confirmation',
	      buttonLabels: ['Cancel', 'Ok'],
	      animation: 'default', // or 'none'
	      primaryButtonIndex: 1,
	      cancelable: true,
	      callback: function(index) {
	      	if (index==1)
	      	{
				MediaLibrary.deleteFileIds = [];
				for(var i = 0; i < MediaLibrary.sortedData.length; i++)
				{
					for(var j = 0; j < MediaLibrary.sortedData[i].data.length; j++)
					{
						for(var k = 0; k < MediaLibrary.sortedData[i].data[j].length; k++)
						{
							if (MediaLibrary.sortedData[i].data[j][k].selected) MediaLibrary.deleteFileIds.push(MediaLibrary.sortedData[i].data[j][k].id);
						}
					}
				}
				$scope.selectCancelClicked();

		      spinnerplugin.show();

		      $http({
		          method: 'POST',
		          url: Url.api + '/user/' + User.id + '/delete-media',
		          data: {
		            file_ids : MediaLibrary.deleteFileIds
		          },                    
		      }).success(function(data){
		        spinnerplugin.hide();
		        MediaLibrary.deleteFiles();
		      });
	      	}
	      }
	  });
	}
	
	$rootScope.bottomBarShareClicked = function()
	{
		$rootScope.sharePopover.show('#bottomBarShare');
	}

	$rootScope.bottomBarForwardClicked = function()
	{
		$rootScope.forwardPopover.show('#bottomBarForward');
	}

	$rootScope.forwardFlique = function()
	{
		$rootScope.forwardPopover.hide();
		Friend.forwardFileIds = [];
		for(var i = 0; i < MediaLibrary.sortedData.length; i++)
		{
			for(var j = 0; j < MediaLibrary.sortedData[i].data.length; j++)
			{
				for(var k = 0; k < MediaLibrary.sortedData[i].data[j].length; k++)
				{
					if (MediaLibrary.sortedData[i].data[j][k].selected) Friend.forwardFileIds.push(MediaLibrary.sortedData[i].data[j][k].id);
				}
			}
		}
		$scope.selectCancelClicked();
		$rootScope.Navigation.goToPage('templates/friends/forward.html');	
	}
});