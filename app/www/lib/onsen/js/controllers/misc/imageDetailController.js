module.controller('ImageDetailController', function($scope, $rootScope, $window, ImageDetail, User, Friend, MediaLibrary, Url, $http){
	$rootScope.ImageDetail = ImageDetail;
	$rootScope.bottomBarDeleteClicked = function()
	{
	    ons.notification.confirm({
	      message: 'Are you sure you want to delete?',
	      // or messageHTML: '<div>Message in HTML</div>',
	      title: 'Confirmation',
	      buttonLabels: ['Cancel', 'Ok'],
	      animation: 'default', // or 'none'
	      primaryButtonIndex: 1,
	      cancelable: true,
	      callback: function(index) {
	      	if (index==1)
	      	{
				MediaLibrary.deleteFileIds = [ImageDetail.data.id];
		      spinnerplugin.show();

		      $http({
		          method: 'POST',
		          url: Url.api + '/user/' + User.id + '/delete-media',
		          data: {
		            file_ids : MediaLibrary.deleteFileIds
		          },                    
		      }).success(function(data){
		        spinnerplugin.hide();
		        MediaLibrary.deleteFiles();
		        $rootScope.Navigation.goBack();
		      });
	      	}
	      }
		});
	}

	$rootScope.bottomBarShareClicked = function()
	{
		$rootScope.sharePopover.show('#bottomBarShare');
	}

	$rootScope.bottomBarForwardClicked = function()
	{
		$rootScope.forwardPopover.show('#bottomBarForward');
	}

	$rootScope.bottomBarSetAsClicked = function()
	{
		$rootScope.setAsPopover.show('#bottomBarSetAs');
	}

	$rootScope.forwardFlique = function()
	{
		$rootScope.forwardPopover.hide();
		Friend.forwardFileIds = [ImageDetail.data.id];
		$rootScope.Navigation.goToPage('templates/friends/forward.html');	
	}

	$rootScope.forwardEmail = function()
	{
		$scope.forwardPopover.hide();
	}

	$rootScope.shareFacebook = function()
	{
		$rootScope.sharePopover.hide();
	}

	$rootScope.shareTwitter = function()
	{
		$rootScope.sharePopover.hide();
	}

	$rootScope.setAsProfileImage = function()
	{
		$rootScope.setAsPopover.hide();
	    spinnerplugin.show();
        User.update({
        	profileImage : ImageDetail.data.file_location
        }).success(function(data){
            spinnerplugin.hide();
            ons.notification.alert({
            	title : 'Set as profile image',
			    message: 'Your profile image has been changed'
			  });
        }).error(function(error){
            spinnerplugin.hide();
            ons.notification.alert({
            	title : 'Error',
			    message: 'An error has occured'
			  });
        });
	}

	$rootScope.setAsBackgroundImage = function()
	{
		$rootScope.setAsPopover.hide();
	    spinnerplugin.show();
        User.update({
        	coverImage : ImageDetail.data.file_location
        }).success(function(data){
            spinnerplugin.hide();
            ons.notification.alert({
            	title : 'Set as background image',
			    message: 'Your background image has been changed'
			  });
        }).error(function(error){
            spinnerplugin.hide();
            ons.notification.alert({
            	title : 'Error',
			    message: 'An error has occured'
			  });
        });
	}

});