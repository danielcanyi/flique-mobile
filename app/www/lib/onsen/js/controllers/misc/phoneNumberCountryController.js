module.controller('PhoneNumberCountryController', function($scope, $rootScope, $window, PhoneNumberEdit, Country){
  $rootScope.menuSwipeable = PhoneNumberEdit.menuSwipeable;
  $scope.locations = Country.countries;
	$scope.locationClicked = function(location){
		PhoneNumberEdit.country = location;
		$rootScope.Navigation.goBack();
	}
});