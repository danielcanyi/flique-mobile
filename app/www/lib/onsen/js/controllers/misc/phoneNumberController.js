module.controller('PhoneNumberController', function($scope, $rootScope, $window, PhoneNumberEdit, User, $http, Url){
  $rootScope.menuSwipeable = PhoneNumberEdit.menuSwipeable;
  	$scope.phoneNumber = PhoneNumberEdit.phoneNumber;
	$scope.phoneNumberCountryCodeText = PhoneNumberEdit.country ? (PhoneNumberEdit.country.name + " (+" + PhoneNumberEdit.country.code + ")") : null;
	$scope.phoneNumberOkClicked = function(){

		if( ! $scope.phoneNumber.match(/^[\s()+-]*([0-9][\s()+-]*){6,20}$/))  
	    {  
            ons.notification.alert({
            	title : 'Phone Number',
			    message: 'Please fill in a valid phone number'
			  });
	      return;
	    }

		PhoneNumberEdit.phoneNumber = $scope.phoneNumber;
    	spinnerplugin.show();
    	$http({
	            method: 'POST',
	            url: Url.api + '/phone-number-update',
	            data: {
	            	user_id : User.id,
	            	user_phone_country : (PhoneNumberEdit.country ? PhoneNumberEdit.country.alpha2 : null),
	            	user_phone : PhoneNumberEdit.phoneNumber
	            }                    
	        }).success(function(data){
		    	spinnerplugin.hide();
	        	data = data.results[0];
	        	if (data.status == 'SUCCESS')
	        	{
	        		//for now, skip validation
	        		spinnerplugin.show();
			        User.get(User.id).success(function(data){
			            User.cache(data.results[0].data);
				    	User.update({phoneNumber : PhoneNumberEdit.phoneNumber, phoneNumberCountry : PhoneNumberEdit.country})
				    	.success(function(data){
					    	spinnerplugin.hide();
					    	User.phoneNumber = User.editData.phoneNumber = PhoneNumberEdit.phoneNumber;
					    	User.phoneNumberCountry = User.editData.phoneNumberCountry = PhoneNumberEdit.country;
				            ons.notification.alert({
				            	title : 'Phone Number',
							    message: 'Your changes have been saved',
							    callback : function()
							    {
							    	$rootScope.Navigation.goToPage(PhoneNumberEdit.toPage);
							    }
							  });
				    	});

			        });
			        return;

					$rootScope.Navigation.goToPage('templates/misc/phoneVerification.html');
	        	}
	        	else alert('Error')
	        })
	        .error(function(){
		    	spinnerplugin.hide();
	        	alert('Error');
	        });
	};
	$scope.phoneNumberCountryCodeClicked = function(){
		PhoneNumberEdit.phoneNumber = $scope.phoneNumber; // need to save as user has navigated away
		$rootScope.Navigation.goToPage('templates/misc/phoneNumberCountry.html');
	};
});