module.controller('ChatEditController', function($scope, $rootScope, $window, Friend, $http, Url, User, $timeout, Chat, ImageDetail, ImageUpload){
  $rootScope.menuSwipeable = true
  $scope.Friend = Friend;
  $scope.Chat = Chat;
  $scope.User = User;
  $scope.editMode = true;

  $scope.imageClicked = function()
  {
    $rootScope.imageSelectPopover.show('#profile-name');
  }


	$rootScope.imageSelectSearch = function()
	{
	    $rootScope.imageSelectPopover.hide();
	    ImageDetail.selectClicked = function()
	    {
            Chat.editData.image = ImageDetail.data.file_location;

        	$scope.pendingImageUpload = false;
	        $rootScope.Navigation.goBack();
	        $rootScope.Navigation.goBack();
	    }
	    $rootScope.Navigation.goToPage('templates/misc/imageSearch.html');
	}

    $rootScope.imageSelectClear = function()
    {
        $rootScope.imageSelectPopover.hide();
        $scope.pendingImageUpload = false;
        Chat.editData.image = null;
    }


        document.addEventListener('deviceready', function() {
            $rootScope.imageSelectGallery = function()
            {
                $rootScope.imageSelectPopover.hide();

                navigator.camera.getPicture(
                    function(imageURI){
                        $scope.$apply(function(){
	                    	Chat.editData.image = imageURI;
	                    	$scope.pendingImageUpload = true;
                        });
                    },
                    function(message) {},
                    { 
                        quality: 50
                        , destinationType: navigator.camera.DestinationType.FILE_URI
                        , sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY 
                    }
                );
            }

            $rootScope.imageSelectCamera = function()
            {
                $rootScope.imageSelectPopover.hide();

                navigator.camera.getPicture(
                    function(imageURI){
                        $scope.$apply(function(){
	                    	Chat.editData.image = imageURI;
	                    	$scope.pendingImageUpload = true;
                        });
                    },
                    function(message) {},
                    { 
                        quality: 50
                        , destinationType: navigator.camera.DestinationType.FILE_URI
                        , sourceType: navigator.camera.PictureSourceType.CAMERA
                        , correctOrientation: true
                    }
                );
            }
        });

    $scope.removeFriend = function(userId)
    {
        var newUserIds = [];
        var newUsers = [];
        for(var i = 0; i < Chat.editData.userIds.length; i++)
        {
            if (Chat.editData.userIds[i] == userId) continue;
            newUserIds.push(Chat.editData.userIds[i]);
            newUsers.push(Chat.editData.users[i]);
        }
        Chat.editData.userIds = newUserIds;
        Chat.editData.users = newUsers;
    };

	var editStep2 = function()
	{
	    $http({
	        method: 'POST',
	        url: Url.api + '/user/' + User.id + '/chat/' + Chat.data.id + '/edit',
	        data : {
	        	title : Chat.editData.title
	        	, users : Chat.editData.userIds
	        	, image : Chat.editData.image
	        	, allow_add : Chat.editData.allowAdd
                , muted : Chat.editData.muted
	        }
	    })
	    .success(function(data){
	        spinnerplugin.hide();

		    Chat.data = data.results[0]
		    Chat.title = Chat.editData.title;
            $rootScope.Navigation.goToPage('templates/chat/chat.html');
            $rootScope.Navigation.popBackStack();
            $rootScope.Navigation.popBackStack();
		});		
	};
	
    $scope.editClicked = function(){
    	if ( ! Chat.editData.title)
    	{
    		alert('Please enter a group name.');
    		return;
    	}
        if (Chat.editData.userIds.length < 2)
        {
          ons.notification.alert({
            message: 'Please add a friend to this chat.'
          });
            return;   
        }
        spinnerplugin.show();
        //check if images need uploading
        if ($scope.pendingImageUpload)
        {
            ImageUpload.uploadDone = function(data){
                Chat.editData.image = data.response;
                editStep2();
            };

            var options = new FileUploadOptions();
            options.fileKey="file";
            options.fileName=Chat.editData.image.substr(Chat.editData.image.lastIndexOf('/')+1);
            options.mimeType="image/jpeg";

            var params = new Object();
            params.value1 = "test";
            params.value2 = "param";

            options.params = params;
            options.chunkedMode = false;

            ImageUpload.startUpload(
                Chat.editData.image
                , function(error){
                    alert("An error has occurred: Code = " + error.code);
                }
            , options);
        }
        else editStep2();
    }

    $scope.exitClicked = function()
    {
        ons.notification.confirm({
          message: 'Are you sure you want to exit?',
          callback: function(idx) {
            switch(idx) {
              case 0:
                break;
              case 1:
                {
                  spinnerplugin.show();
                  $http({
                          method: 'POST',
                          url: Url.api + '/user/' + User.id + '/chat/leave',
                          data : {chat_ids : [Chat.data.id]}
                      }).success(function(data){
                        spinnerplugin.hide();
                        var newChats = [];
                        for(var i = 0; i < Chat.chats.length; i++)
                        {
                          if (Chat.chats[i].id != Chat.data.id) newChats.push(Chat.chats[i]);

                        }
                        Chat.chats = newChats;
                        $rootScope.Navigation.goBack();
                        $rootScope.Navigation.goBack();
                      });           
                }
                break;
            }
          }
        });   
    }
});