module.controller('ChatController', function($scope, $rootScope, $window, Url, $http, Chat, User, $timeout, $document, $anchorScroll, $location, ImageDetail, FliqueNotification){
	$scope.Chat = Chat;
	var boxWidth = 300;
	$scope.chat_box_margin_left = (window.innerWidth - boxWidth)/2;

	var clearNotifications = function(){
		if ( ! FliqueNotification.data.chats[Chat.currId]) return;
		var notificationIds = [];
		for( var i = 0; i < FliqueNotification.data.chats[Chat.currId].length; i++)
		{
			notificationIds.push(FliqueNotification.data.chats[Chat.currId][i].id);
		}
		FliqueNotification.clear(notificationIds);
		FliqueNotification.data.chats[Chat.currId] = [];
	}
	$rootScope.scrollDown = function()
	{
		$timeout(function(){
	      // set the location.hash to the id of
	      // the element you wish to scroll to.
	      $location.hash('bottom');

	      // call $anchorScroll()
	      $anchorScroll();
		},10);
	}

	if (Chat.loadFriend)
	{
	    spinnerplugin.show();
	    $http({
	        method: 'GET',
	        url: Url.api + '/user/' + User.id + '/friend/' + Chat.friendId + "/chat"})
	    .success(function(data){
		    spinnerplugin.hide();
	    	Chat.data = data.results[0];
	    	Chat.currId = data.id;
	    	clearNotifications();
			Chat.crunchData();
			$rootScope.scrollDown();
			Chat.loadFriend = false;
			Chat.loadChat = true;
			Chat.invokeReloadLoop(true);
		});
	}
	else {
		Chat.currId = Chat.data.id;
    	clearNotifications();
		Chat.forceScrollDown = true;
		Chat.invokeReloadLoop(false);
	}

	$scope.messageFileClicked = function(file)
	{
		ImageDetail.data = file;
		ImageDetail.title = "Image " + file.id;
		ImageDetail.resetBar();
		ImageDetail.hideDelete = true;
		ImageDetail.showOptionsBar = true;
		$rootScope.Navigation.goToPage('templates/misc/imageDetail.html');
	}

	$scope.edit = function()
	{
		Chat.editData.title = Chat.data.title;
		Chat.editData.image = Chat.data.image;
		Chat.editData.allowAdd = Chat.data.allow_add;
		Chat.editData.users = [];
		Chat.editData.userIds = [];
		Chat.afterSelect = null;
		for(var i = 0; i < Chat.data.users.length; i++)
		{
			if (Chat.data.users[i].user_id == User.id)
			{
				Chat.editData.muted = Number(Chat.data.users[i].muted) == 1;
			}
			Chat.editData.users.push(Chat.data.users[i].user);
			Chat.editData.userIds.push(Chat.data.users[i].user_id);
		}
		$rootScope.Navigation.goToPage('templates/chat/edit.html');
	}

	$rootScope.sendClicked = function()
	{
		var value = document.querySelector('#chatText').value;
		if (value.length < 1) return;
	    $http({
	        method: 'POST',
	        url: Url.api + '/user/' + User.id + '/chat/' + Chat.data.id + "/message",
	        data : {text : value}
	    })
	    .success(function(data){
		});
		document.querySelector('#chatText').value = '';
	}
});