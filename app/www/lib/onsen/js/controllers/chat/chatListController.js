module.controller('ChatListController', function($scope, $rootScope, $window, Chat, $http, Url, User, $timeout, FliqueNotification){
  $rootScope.menuSwipeable = true
  $scope.search = "";

  $scope.Chat = Chat;
  $scope.FliqueNotification = FliqueNotification;
  $scope.deleteIds = [];

  $scope.$watch('search', function(){
    if ( ! Chat.chats) return;
    for(var i = 0; i < Chat.chats.length; i++)
    {
      Chat.chats[i].searchFound = false;
      var searchTitle = "";
      if (Chat.chats[i].title && Chat.chats[i].title.length > 0) searchTitle = Chat.chats[i].title.toUpperCase();
      if (searchTitle.search($scope.search.toUpperCase()) >-1)
      {
        Chat.chats[i].searchFound = true;
        continue;
      } 

      for(var j = 0; j < Chat.chats[i].users.length; j++)
      {
        var searchName = "";
        if (Chat.chats[i].users[j].user.name && Chat.chats[i].users[j].user.name.length > 0) searchName = Chat.chats[i].users[j].user.name.toUpperCase();
        if (searchName.search($scope.search.toUpperCase()) >-1)
        {
          Chat.chats[i].searchFound = true;
          break;
        } 
      }

    }
  });

  $scope.setGroupsSelected = function(groupsSelected)
  {
    Chat.groupsSelected = groupsSelected;
  }

  Chat.friendChatsCount = Chat.friendChatsCount ? Chat.friendChatsCount : 0;
  Chat.groupChatsCount = Chat.friendChatsCount ? Chat.friendChatsCount : 0;

  var adaptChats = function(data){
    Chat.friendChatsCount = 0;
    Chat.groupChatsCount = 0;
    Chat.chats = [];
    if ( ! data.results) return;
    for(var i = 0; i < data.results.length; i++)
    {
      var chat = data.results[i];
      if (chat.two_way_user_id)
      {
        Chat.friendChatsCount++;
        for(var j = 0; j < chat.users.length; j++)
        {
          if (chat.users[j].user_id == User.id) continue;
          chat.title = chat.users[j].user.name;
          chat.image = chat.users[j].user.profile_image;
        }
        if (chat.messages.length > 0)
        {
          chat.desc = chat.messages[chat.messages.length - 1].file_id ? 'image' : chat.messages[chat.messages.length - 1].text;
        }
      }
      else
      {
        Chat.groupChatsCount++;
        if (chat.messages.length > 0)
        {
          var message = chat.messages[chat.messages.length - 1];
          for(var j = 0; j < chat.users.length; j++)
          {
            if (chat.users[j].user_id == message.user_id)
            {
              message.user = chat.users[j];
              break;
            }
          }
          chat.desc = $rootScope.ucwords(message.user.user.name) + ": " + (message.file_id ? 'image' : message.text);
        }
      }
      Chat.chats.push(chat);
    }
  }

  if (Chat.loadUserCreatedChats)
  {
    $http({
      method: 'GET',
      url: Url.api + '/user/' + User.id + '/chat/created'})
    .success(function(data){
      adaptChats(data);
    });
  }
  else
  {
    $http({
      method: 'GET',
      url: Url.api + '/user/' + User.id + '/chat/list'})
    .success(function(data){
      adaptChats(data);
    });
  }
  FliqueNotification.reload();

  $scope.newClicked = function(){
    Chat.editData.userIds = [];
    Chat.editData.users = [];
    Chat.editData.title = null;
    Chat.editData.allowAdd = false;
    Chat.editData.image = null;
    Chat.afterSelect = 'create';
  	$rootScope.Navigation.goToPage('templates/chat/friendSelect.html');
  }

  $scope.openMenuStr = null;

  $scope.swipeLeft = function(_index)
  {
    $scope.openMenuStr = _index;
  }

  var menuButtonJustClicked = false;
  $scope.chatClicked = function(index)
  {
    if ($scope.deleteMode) return;

    if (menuButtonJustClicked)
    {
      menuButtonJustClicked = false;
      return;
    }

    if ($scope.openMenuStr == index){
      $scope.openMenuStr = null;
      return;
    } 
    Chat.data = {id : Chat.chats[index].id};
    Chat.messages = [];
    Chat.title = Chat.chats[index].title;
    if (Chat.chats[index].two_way_user_id) Chat.friendId = Chat.chats[index].two_way_user_id;
    $rootScope.Navigation.goToPage('templates/chat/chat.html');
  }

  $scope.muteClicked = function(index)
  {
    menuButtonJustClicked = true;
    if (Chat.chats[index].muted == 0)
    {
      $scope.currMuteChatIndex = index;
      $rootScope.mutePopover.show('#mute_' + index);
      return;
    }
    
    spinnerplugin.show();
    //close friend menu
    $scope.openMenuStr = null;
    $http({
            method: 'POST',
            url: Url.api + '/user/' + User.id + '/chat/' + Chat.chats[index].id + '/unmute',
        }).success(function(data){
          spinnerplugin.hide();
          Chat.chats[index].muted = 0;
        });    
  }

  $rootScope.mute = function(days)
  {
    spinnerplugin.show();
    $rootScope.mutePopover.hide();

    //close friend menu
    $scope.openMenuStr = null;
    $http({
            method: 'POST',
            url: Url.api + '/user/' + User.id + '/chat/' + Chat.chats[$scope.currMuteChatIndex].id + '/mute',
            data : {days : days}
        }).success(function(data){
          spinnerplugin.hide();
          Chat.chats[$scope.currMuteChatIndex].muted = 1;
        });
  }

  $scope.deleteClicked = function()
  {
    ons.notification.confirm({
      message: 'Are you sure you want to delete?',
      callback: function(idx) {
        switch(idx) {
          case 0:
            break;
          case 1:
            {
              spinnerplugin.show();
              $http({
                      method: 'POST',
                      url: Url.api + '/user/' + User.id + '/chat/leave',
                      data : {chat_ids : $scope.deleteIds}
                  }).success(function(data){
                    spinnerplugin.hide();
                    var newChats = [];
                    for(var i = 0; i < Chat.chats.length; i++)
                    {
                      if ($scope.deleteIds.indexOf(Chat.chats[i].id) < 0) newChats.push(Chat.chats[i]);

                    }
                    Chat.chats = newChats;
                    $scope.deleteIds = [];
                    $scope.deleteMode = false;
                  });           
            }
            break;
        }
      }
    });
  }

  $scope.deleteSelect = function(id)
  {
    if ( ! $scope.deleteMode) return;
    var index = $scope.deleteIds.indexOf(id);
    if (index > -1) $scope.deleteIds.splice(index, 1);
    else $scope.deleteIds.push(id);
  }

  $scope.deleteModeClicked = function()
  {
    $scope.deleteMode = true;
  }

});