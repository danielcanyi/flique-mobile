module.controller('PhoneVerificationController', function($scope, $rootScope, $window, PhoneNumberEdit, User, $http, Url){
  $rootScope.menuSwipeable = PhoneNumberEdit.menuSwipeable;
    $scope.phoneVerificationConfirmClicked = function(){
    	spinnerplugin.show();

    	$http({
	            method: 'POST',
	            url: Url.api + '/phone-number-update/activation',
	            data: {
	            	activation_code : $scope.code
	            }                    
	        }).success(function(data){
		    	spinnerplugin.hide();
	        	data = data.results[0];
	        	if (data.status == 'SUCCESS')
	        	{
			        User.get(User.id).success(function(data){
			            User.cache(data.results[0].data);
				    	User.update({phoneNumber : PhoneNumberEdit.phoneNumber, phoneNumberCountry : PhoneNumberEdit.country})
				    	.success(function(data){
					    	spinnerplugin.hide();
					    	User.phoneNumber = PhoneNumberEdit.phoneNumber;
					    	User.phoneNumberCountry = PhoneNumberEdit.country;
					    	alert('Your changes have been saved');
					    	$rootScope.Navigation.goToPage(PhoneNumberEdit.toPage);
				    	});

			        });
			    }
	        	else alert('Incorrect code')
	        })
	        .error(function(){
		    	spinnerplugin.hide();
	        	alert('Error');
	        });
    }
});