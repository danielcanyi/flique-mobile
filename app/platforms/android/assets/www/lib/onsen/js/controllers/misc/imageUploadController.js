module.controller('ImageUploadController', function($scope, $http, $rootScope, $window, ImageUpload){
	$scope.imageUploadTitle = ImageUpload.title;
	$scope.choose = function(){
		alert('choose : function to be overridden');
	};
	$scope.use = function(){
		alert('use : function to be overridden');
	};
	$scope.imageURI = null;
	$scope.ImageUpload = ImageUpload;

	document.addEventListener('deviceready', function() {
		$scope.choose = function(){
			ImageUpload.cancelUpload();
	     	navigator.camera.getPicture(
	         	function(imageURI){
		          	$scope.$apply(function(){
			          	$scope.imageURI = imageURI;
		          	});
	         	},
	         	function(message) { alert('get picture failed'); },
	            { 
	            	quality: 50
	            	, destinationType: navigator.camera.DestinationType.FILE_URI
	            	, sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY 
	            }
	        );
		};
		$scope.choose();
		$scope.use = function(){
			/*
			ImageUpload.cancelUpload();
			ImageUpload.uploadDone($scope.imageURI);
			return;
			*/

	        var options = new FileUploadOptions();
	        options.fileKey="file";
	        options.fileName=$scope.imageURI.substr($scope.imageURI.lastIndexOf('/')+1);
	        options.mimeType="image/jpeg";

	        var params = new Object();
	        params.value1 = "test";
	        params.value2 = "param";

	        options.params = params;
	        options.chunkedMode = false;

	        ImageUpload.startUpload(
	        	$scope.imageURI
	        	, function(error){
		            alert("An error has occurred: Code = " + error.code);
		        }
	        , options);
		};
    }, false);

});