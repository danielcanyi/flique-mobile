module.controller('ProfileEditController', function($scope, $rootScope, $window, PhoneNumberEdit, User, $timeout, ImageSelection, ImageUpload, Reset, Friend, $http, Url, ImageDetail){
        $rootScope.menuSwipeable = true;
        $scope.profileShowActions = false;
        $scope.profileShowEdit = true;

        Reset.reset();

        var refresh = function(){
            if (User.phoneNumber)
            {
                $scope.profilePhoneStr = "";
                if (User.phoneNumberCountry && User.editData.phoneNumberCountry != undefined) $scope.profilePhoneStr = "(+" + User.editData.phoneNumberCountry.code + ") ";
                $scope.profilePhoneStr += User.editData.phoneNumber;
            }

            if (User.editData.country) $scope.profileCountryStr = User.editData.country.name;

            if (User.editData.status) $scope.profileStatusStr = User.editData.status;

            if (User.editData.profileImage) $scope.profileImage = User.editData.profileImage;
            if (User.editData.coverImage) $scope.coverImage = User.editData.coverImage;

            if (User.editData.dob)
            {
                var dob = new Date(User.editData.dob);
                var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                $scope.profileDobStr = dob.getDate() + " " + months[dob.getMonth()] + " " + dob.getFullYear();
            }
            
            $scope.profileDisplayStatusStr = 'Tap to change images';
        };

        refresh();

        var refreshEditData = false;
        if (User.editing) User.editing = false;
        else refreshEditData = true;

        var initEditData = function()
        {
            if(refreshEditData)
            {
                for (var key in User.editData) {
                  if ( ! User.editData.hasOwnProperty(key)) continue;
                  User.editData[key] = User[key];
                }
            }
            refresh();
        }

        if (User.cached)
        {
            initEditData();
        }
        else
        {
            User.get(User.id).success(function(data){
                User.cache(data.results[0].data);
                initEditData();
            });
        }

        Friend.upload();

        var bgClickBlocked = false;
        var unblockBgClick;
        $scope.blockBgClick = function(){
            $timeout.cancel(unblockBgClick);
            bgClickBlocked = true;
            unblockBgClick = $timeout(function(){bgClickBlocked = false;}, 100);
        };

        var imageSelectFor = "";
        $scope.profileImageClicked = function(){
            User.editing = true;
            $scope.blockBgClick();
            imageSelectFor = "PROFILE_IMAGE";
            $rootScope.imageSelectPopover.show('#profile-image');
        };
        $scope.profileBgClicked = function(){
            User.editing = true;
            if (bgClickBlocked) return;
            imageSelectFor = "PROFILE_BG";
            $rootScope.imageSelectPopover.show('#profile-background');
        };

        $scope.bgPendingUpload = false;
        //for browser testing only
        $rootScope.bgEditGallery = function(){
            $rootScope.imageSelectPopover.hide();
        }

        $scope.profileDobClicked = function(){
            User.editing = true;
            $rootScope.Navigation.goToPage('templates/profile/dobEdit.html');
        };

        document.addEventListener('deviceready', function() {
            $rootScope.imageSelectGallery = function()
            {
                $rootScope.imageSelectPopover.hide();

                navigator.camera.getPicture(
                    function(imageURI){
                        $scope.$apply(function(){
                            if (imageSelectFor == "PROFILE_BG")
                            {
                                $scope.coverImage = imageURI;
                                User.editData.coverImage = imageURI;
                                $scope.bgPendingUpload = true;
                            } 
                            else if (imageSelectFor == "PROFILE_IMAGE") 
                            {
                                $scope.profileImage = imageURI;
                                User.editData.profileImage = imageURI;
                                $scope.profilePicPendingUpload = true;
                            }
                        });
                    },
                    function(message) {},
                    { 
                        quality: 50
                        , destinationType: navigator.camera.DestinationType.FILE_URI
                        , sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY 
                    }
                );
            }

            $rootScope.imageSelectCamera = function()
            {
                $rootScope.imageSelectPopover.hide();

                navigator.camera.getPicture(
                    function(imageURI){
                        $scope.$apply(function(){
                            if (imageSelectFor == "PROFILE_BG")
                            {
                                $scope.coverImage = imageURI;
                                User.editData.coverImage = imageURI;
                                $scope.bgPendingUpload = true;
                            } 
                            else if (imageSelectFor == "PROFILE_IMAGE") 
                            {
                                $scope.profileImage = imageURI;
                                User.editData.profileImage = imageURI;
                                $scope.profilePicPendingUpload = true;
                            }
                        });
                    },
                    function(message) {},
                    { 
                        quality: 50
                        , destinationType: navigator.camera.DestinationType.FILE_URI
                        , sourceType: navigator.camera.PictureSourceType.CAMERA
                        , correctOrientation: true
                    }
                );
            }

            $scope.profileDobClicked = function(){
                datePicker.show({
                    date : new Date(User.editData.dob),
                    mode : 'date'
                },function(date){
                  var padDateVal = function(num)
                  {
                    return ((num < 10) ? "0" : "") + num;
                  };
                  User.editData.dob = date.getFullYear() + "-" + padDateVal(date.getMonth() + 1) + "-" + padDateVal(date.getDate());
                  $scope.$apply(function(){
                      refresh();
                  });
                });
            };

        }, false);

        $rootScope.imageSelectSearch = function()
        {
            $rootScope.imageSelectPopover.hide();
            ImageDetail.selectClicked = function()
            {
                if (imageSelectFor == "PROFILE_BG")
                    User.editData.coverImage = ImageDetail.data.file_location;
                else if (imageSelectFor == "PROFILE_IMAGE")
                    User.editData.profileImage = ImageDetail.data.file_location;

                $rootScope.Navigation.goBack();
                $rootScope.Navigation.goBack();
            }
            $rootScope.Navigation.goToPage('templates/misc/imageSearch.html');
        }

        $scope.profileCountryClicked = function(){
            User.editing = true;
            $rootScope.Navigation.goToPage('templates/profile/country.html');
        };
        $scope.profilePhoneClicked = function(){
            User.editing = true;
            PhoneNumberEdit.menuSwipeable = true;
            PhoneNumberEdit.phoneNumber = User.phoneNumber;
            PhoneNumberEdit.country = User.phoneNumberCountry;
            $rootScope.Navigation.goToPage('templates/misc/phoneNumber.html');
        };
        $scope.profileStatusClicked = function(){
            User.editing = true;
            $rootScope.Navigation.goToPage('templates/profile/statusEdit.html');
        };
        var updateStep3 = function(){
            //force name to confirm to ucwords
            User.editData.name = $rootScope.ucwords(User.editData.name);
            User.update(User.editData).success(function(data){
                spinnerplugin.hide();
                $rootScope.Navigation.goToPage('templates/profile/profile.html', true);
            }).error(function(error){
                spinnerplugin.hide();
                alert('an error has ocurred');
            });
        };
        var updateStep2 = function(){
            if ($scope.profilePicPendingUpload)
            {
                ImageUpload.uploadDone = function(data){
                    User.editData.profileImage = data.response;
                    updateStep3();
                };

                var options = new FileUploadOptions();
                options.fileKey="file";
                options.fileName=User.editData.profileImage.substr(User.editData.profileImage.lastIndexOf('/')+1);
                options.mimeType="image/jpeg";

                var params = new Object();
                params.value1 = "test";
                params.value2 = "param";

                options.params = params;
                options.chunkedMode = false;

                ImageUpload.startUpload(
                    User.editData.profileImage
                    , function(error){
                        alert("An error has occurred: Code = " + error.code);
                    }
                , options);
            }
            else updateStep3();
        };
        $scope.profileUpdateClicked = function(){
            User.profileCreateMode = false;
            spinnerplugin.show();
            //check if images need uploading
            if ($scope.bgPendingUpload)
            {
                ImageUpload.uploadDone = function(data){
                    User.editData.coverImage = data.response;
                    updateStep2();
                };

                var options = new FileUploadOptions();
                options.fileKey="file";
                options.fileName=User.editData.coverImage.substr(User.editData.coverImage.lastIndexOf('/')+1);
                options.mimeType="image/jpeg";

                var params = new Object();
                params.value1 = "test";
                params.value2 = "param";

                options.params = params;
                options.chunkedMode = false;

                ImageUpload.startUpload(
                    User.editData.coverImage
                    , function(error){
                        alert("An error has occurred: Code = " + error.code);
                    }
                , options);
            }
            else updateStep2();
        }
        $scope.profileCancelClicked = function(){
            $rootScope.Navigation.goToPage('templates/profile/profile.html', true);
        };

    $scope.User = User;
});