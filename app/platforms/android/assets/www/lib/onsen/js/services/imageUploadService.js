module.factory('ImageUpload', function($http, $rootScope, Url, User) {
	var self = {
		title : null,
		uploadDone : function(r){
		},
		ft : null,
		uploading : false,
		startUpload : function(imageURI, error, options){
			self.uploading = true;
		},
		cancelUpload : function(){
			self.uploading = false;
			if (self.ft) self.ft.abort();
		},
		reset : function(){
			self.title = null;
			self.cancelUpload();
			self.uploadDone = function(r){};
		}
	};
	document.addEventListener('deviceready', function() {
		self.startUpload = function(imageURI, error, options){

			self.uploading = true;
	
			self.ft = new FileTransfer();
	        self.ft.upload(imageURI, encodeURI(Url.api + "/user/" + User.id +  "/upload_non_ebengate")
	        	, function(r){
		        	self.uploadDone(r);
					self.uploading = false;
		        }
		        , function(errorData){
	        		alert('upload error');
		        	error(errorData);
					self.uploading = false;
		        }
	        , options);

		};
	}, false);
	return self;
});