module.factory('ImageSelection', function($http, $timeout, $location, $window) {
	var self = {
		data :[],
		title : null,
		selected : [],
		selectionOk : false,
		reset : function(){ 
			self.selected = []; 
			self.selectionOk = false;}
	};
	return self;
});