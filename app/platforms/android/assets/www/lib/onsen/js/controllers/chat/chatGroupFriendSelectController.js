module.controller('ChatGroupFriendSelectController', function($scope, $rootScope, $window, Friend, $http, Url, User, $timeout, Chat){
  $rootScope.menuSwipeable = true
  $scope.Friend = Friend;
  $scope.search = "";

  $scope.$watch('search', function(){
  	Friend.friendsSorted = Friend.applySearch(Friend.friendsSorted, $scope.search);
  });

  Friend.selectedIds = [];
  for(var i = 0; i < Chat.editData.userIds.length; i++)
  {
    Friend.selectedIds.push(Chat.editData.userIds[i]);
  }
  Friend.selectReload();

  $scope.nextClicked = function()
  {
    Chat.editData.userIds = Friend.selectedIds;
    Chat.editData.users = [];

    for(var key in Friend.friendsSorted)
    {
      if ( ! Friend.friendsSorted.hasOwnProperty(key)) continue;
      for(var i = 0; i < Friend.friendsSorted[key].length; i++)
      {
        if (Friend.selectedIds.indexOf(Friend.friendsSorted[key][i].id) > -1) Chat.editData.users.push(Friend.friendsSorted[key][i]);
      }
    }
    if (Chat.afterSelect) $rootScope.Navigation.goToPage('templates/chat/' + Chat.afterSelect + '.html');
    else $rootScope.Navigation.goBack();
  }
});