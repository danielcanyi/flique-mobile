module.controller('ProfileStatusEditController', function($scope, $rootScope, $window, User, Fb, $http, Url){
  $rootScope.menuSwipeable = true;
  $scope.postFb = false;
	var next = function(){
    	User.editData.status = $scope.statusEditStatusStr;
    	$rootScope.Navigation.goBack();
	};

	$scope.postFbToggle = function(){
		if ($scope.postFb)
		{
			$scope.postFb = false;
			return;
		}
		if ( ! User.facebook_id)
		{

	    	Fb.loginSuccess = function(userData){

		    	spinnerplugin.show();
		    	$http({
		            method: 'POST',
		            url: Url.api + '/sign-in/fb-sign-in',
		            data: {
		            	fb_user_email : userData.email,
		            	fb_user_id : userData.id,
		            	name : userData.name
		            }                    
		        }).success(function(data){
			    	spinnerplugin.hide();
		        	data = data.results[0];
		        	if (data.status == 'SUCCESS')
		        	{
		        		User.cache(data.message[0]);
		        		$scope.postFb = true;
		        	}
		        })
		        .error(function(){
			    	spinnerplugin.hide();
		        	alert('Error');
		        });
		    };

	    	Fb.login();
		}
		else $scope.postFb = true;
	};
  $scope.statusEditStatusStr = User.editData.status;
   $scope.$watch('statusEditStatusStr', function() {
   		$scope.statusEditStatusStr = $scope.statusEditStatusStr.replace(/[\n\r]/g, ' ');
   		if ($scope.statusEditStatusStr.length > 140)
   		{
   			$scope.statusEditStatusStr = $scope.statusEditStatusStr.substring(0, 140);
   		}
   });
	$scope.statusEditOkClicked = function(){
		if ( ! $scope.postFb)
		{
			next();
			return;
		}
		Fb.postSuccess = function(){next();};
		Fb.post($scope.statusEditStatusStr);
	}
});