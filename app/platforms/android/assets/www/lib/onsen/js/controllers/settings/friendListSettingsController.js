module.controller('FriendListSettingsController', function($scope, $rootScope, $window, Friend){
  $rootScope.menuSwipeable = true
  $scope.Friend = Friend;
	Friend.contactsSorted = Friend.sort(Friend.contacts);
  $scope.search = "";

  $scope.$watch('search', function(){
  	Friend.contactsSorted = Friend.applySearch(Friend.contactsSorted, $scope.search);
  });
});