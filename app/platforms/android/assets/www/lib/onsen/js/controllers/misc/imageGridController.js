module.controller('ImageGridController', function($scope, $rootScope, $window, ImageSelection){
	$scope.imageGridTitle = ImageSelection.title;
	var newImageGridData = [];
	for(var i = 0; i < ImageSelection.data.length; i++)
	{
		if (i%3 == 0) newImageGridData.push([]);
		newImageGridData[window.Math.floor(i/3)].push(ImageSelection.data[i]);
	}
	$scope.imageGridData = newImageGridData;
	$scope.imageGridImageClicked = function(image){
		ImageSelection.selected = [image];
		$rootScope.Navigation.goToPage('templates/misc/imageDetail.html');
	};
});