module.controller('ProfileCountryController', function($scope, $rootScope, $window, Country, User){
  $rootScope.menuSwipeable = true;
  $scope.locations = Country.countries;
	$scope.locationClicked = function(location){
    	User.editData.country = location;
    	$rootScope.Navigation.goBack();
	}
});