module.factory('Friend', function($http, $timeout, $location, $window, Url, User, $timeout) {
	var self = {
		contacts :[{name : "asd", phone : 1234, status :'status'}
				,{name : "asd", phone : 1234}
				,{name : "asd", phone : 1234}
				,{name : "asd", phone : 1234}]
		, friends : []
		, friendsSorted : {}
		, contactsSorted : {}
		, suggestions : []
		, suggestionsSorted : {}
		, requests : []
		, requestsSorted : {}
		, requestsSent : []
		, requestsSentSorted : {}
		, currFriendId : null
		, currMutualFriends : []
		, currMutualFriendsSorted : {}
		, forwardFileIds : []
		, selectedIds : []
		, selectItemClicked : function(id){
			if ( ! self.selectedIds) self.selectedIds = [];
		    var index = self.selectedIds.indexOf(id);
		    if (index > -1) self.selectedIds.splice(index, 1);
		    else self.selectedIds.push(id);
		}
		, selectReload : function(){
		    spinnerplugin.show();
		    $http({
		      method: 'GET',
		      url: Url.api + '/user/' + User.id + '/get-all-friends'})
		    .success(function(data){
		      data = data.results[0];
		      if (data.status == 'SUCCESS')
		      {
		        self.friends = [];
		        for(var i = 0; i < data.friends.length; i++)
		        {
		          self.friends.push({
		            id : data.friends[i].friend_user_id
		            , user_id : data.friends[i].friend_user_id
		            , name : data.friends[i].friend_name
		            , status : data.friends[i].friend_profile_status
		            , photos : [data.friends[i].friend_profile_image]
		            , muted : data.friends[i].muted
		          });
		        }
		      self.friendsSorted = self.sort(self.friends);
		      }
		      spinnerplugin.hide();
		    });
		}
		, chars : ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','#']
		, sort : function(data){
			var retval = {};
			for(var i = 0; i < data.length; i++)
			{
				var firstChar = "#";
				if (data[i].name && data[i].name.length > 0) firstChar = data[i].name.substring(0,1).toUpperCase();
				if (self.chars.indexOf(firstChar) < 0) firstChar = '#';
				if ( ! retval[firstChar]) retval[firstChar] = [];
				retval[firstChar].push(data[i]);
			};
			return retval;
		}
		, applyToAll : function(data, appliedFunction)
		{
			var retval = {};
			for(var key in data)
			{
				if ( ! data.hasOwnProperty(key)) continue;
				retval[key] = data[key];
				for(var i = 0; i < retval[key].length; i++)
				{
					retval[key][i] = appliedFunction(retval[key][i]);
				}
			}
			return retval;
		}
		, applySearch : function(data, search){
			return self.applyToAll(data,
				function(element)
				{
			  		element.searchFound = false;
			  		if ( ! search) return element;
			  		var searchName = "";
			  		if (element.name && element.name.length > 0) searchName = element.name.toUpperCase();
			  		if (searchName.search(search.toUpperCase()) >-1) element.searchFound = true;

			  		return element;
				});
		}
		, upload : function(){
	      return $http({
	          method: 'POST',
	          url: Url.api + '/contacts/' + User.id,
	          data: {
	              contacts : self.contacts
	          },                    
	     	});
		}
			
	};

	return self;
});