module.factory('FliqueNotification', function($http, $timeout, $location, $window, User, Url, $rootScope, $timeout) {
	var self = {
		reload : function()
		{
		    $http({
		        method: 'GET',
		        url: Url.api + '/user/' + User.id + '/notifications'})
		    .success(function(data){
		    	self.data = {chats : {}};
		    	var data = data.results;
		    	for(var i = 0; i < data.chats.length; i ++)
		    	{
		    		if ( ! self.data.chats[data.chats[i].chat_id]) self.data.chats[data.chats[i].chat_id] = [];
		    		self.data.chats[data.chats[i].chat_id].push(data.chats[i]);
		    	}
		    });
		},
		clear : function(ids)
		{
			$http({
				method : 'POST',
				url : Url.api + '/user/' + User.id + '/clear-notifications',
				data : { notification_ids : ids }
			})
			.success(function(data){
				console.log(ids);
			});
		}
	}
	return self;
});