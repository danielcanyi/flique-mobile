module.controller('FriendProfileController', function($scope, $rootScope, $window, $http, Reset, Friend, Url, Country, User, MediaLibrary, Chat){
    $rootScope.menuSwipeable = true;
    $scope.profileShowLocation = true;
    $scope.profileShowActions = true;
    $scope.profileShowMutual = true;
    $scope.profileShowFriendChat = true;

    $scope.mutualGroupChatsCount = 0;
    $scope.mediaExchangedCount = 0;

    $scope.Friend = Friend;

    Reset.reset();

    $http({
        method: 'GET',
        url: Url.api + '/user/' + Friend.currFriendId + '/details'})
    .success(function(data){
        data = data.results[0].data;
        if (data.name) $scope.profileDisplayUsernameStr = $scope.profileUsernameStr = data.name;
        if (data.profile_status) $scope.profileDisplayStatusStr = $scope.profileStatusStr = data.profile_status;
        var country = null;
        var phoneNumberCountry = null;
        for(var i = 0; i < Country.countries.length; i++)
        {
            if (Country.countries[i].alpha2 == data.user_country)
            {
                country = Country.countries[i];
            }
            if (Country.countries[i].alpha2 == data.user_phone_country)
            {
                phoneNumberCountry = Country.countries[i];
            }
        }
        if (country) $scope.profileCountryStr = country.name;
        else $scope.profileShowLocation = false;

        if (data.user_phone)
        {
            $scope.profilePhoneStr = "";
            if (phoneNumberCountry && phoneNumberCountry != undefined) $scope.profilePhoneStr = "(+" + phoneNumberCountry.code + ") ";
            $scope.profilePhoneStr += data.user_phone;
            $scope.profileShowContact = true;
        }
        else $scope.profileShowContact = false;
        if (data.profile_image) $scope.profileImage = data.profile_image;
        if (data.cover_image) $scope.coverImage = data.cover_image;
        if (data.dob)
        {
            var dob = new Date(data.dob);
            var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            $scope.profileDobStr = dob.getDate() + " " + months[dob.getMonth()] + " " + dob.getFullYear();
            $scope.profileBirthdayStr = dob.getDate() + " " + months[dob.getMonth()];
        }
        
        $http({
            method: 'GET',
            url: Url.api + '/user/' + User.id + '/friend/get_friendship/' + Friend.currFriendId})    
        .success(function(data){
            $scope.friendship = data.results[0];
        });
    });

    
    $http({
        method: 'GET',
        url: Url.api + '/user/' + User.id + '/get-mutual-friends/' + Friend.currFriendId})
    .success(function(data){
        data = data.results[0];
        Friend.currMutualFriends = [];
        for(var i = 0; i < data.friends.length; i++)
        {
            Friend.currMutualFriends.push({
                id : data.friends[i].friend_user_id
                , name : data.friends[i].friend_name
                , status : data.friends[i].friend_profile_status
                , photos : [data.friends[i].friend_profile_image]
                , muted : data.friends[i].muted
            });
        }
        $http({
            method: 'GET',
            url: Url.api + '/user/' + User.id + '/friend/' + Friend.currFriendId + '/get-media-exchanged-count'})
            .success(function(data){
                $scope.mediaExchangedCount = data.results.count;
            });  

        Friend.currMutualFriendsSorted = Friend.sort(Friend.currMutualFriends);
    });

    $scope.muteClicked = function()
    {
      $rootScope.mutePopover.show('#muteButton');
    }

    $scope.unmuteClicked = function()
    {

        spinnerplugin.show();
        //close friend menu
        $http({
                method: 'POST',
                url: Url.api + '/user/' + User.id + '/friend/unmute/' + Friend.currFriendId,
            }).success(function(data){
              spinnerplugin.hide();
              $scope.friendship.muted = 0;
            });   
    }
      $rootScope.mute = function(days)
      {
        spinnerplugin.show();
        $rootScope.mutePopover.hide();

        $http({
                method: 'POST',
                url: Url.api + '/user/' + User.id + '/friend/mute/' + Friend.currFriendId,
                data : {days : days}
            }).success(function(data){
              spinnerplugin.hide();
              $scope.friendship.muted = 1;
            });
      }


    $scope.chatClicked = function()
    {
        Chat.data = null;
        Chat.friendId = Friend.currFriendId;
        Chat.loadFriend = true;
        Chat.loadChat = false;
        $rootScope.Navigation.goToPage('templates/chat/chat.html');
    }


    $scope.mediaExchangedClicked = function(){
        spinnerplugin.show();
        $http({
            method: 'GET',
            url: Url.api + '/user/' + User.id + '/friend/' + Friend.currFriendId + '/get-media-exchanged'})
            .success(function(data){
                spinnerplugin.hide();
                MediaLibrary.data = data.results ? data.results : [];
                MediaLibrary.title = "Media exchanged";
                $rootScope.Navigation.goToPage('templates/misc/mediaLibrary.html');
            });
    };
});