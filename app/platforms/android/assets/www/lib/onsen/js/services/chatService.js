module.factory('Chat', function($http, $timeout, $location, $window, User, Url, $rootScope, $timeout) {
	var self = {
		afterSelect : 'create'
		,editData : {
			userIds : []
			, users : []
			, image : null
			, title : null
			, allowAdd : false
		}
		,reloadLoopOn : false
		,currId : null
		,crunchData: function()
		{
			self.users = {};
			for(var i = 0; i < self.data.users.length; i++)
			{
				self.users[self.data.users[i].user_id] = self.data.users[i].user;
			}

			self.messages = [];
			if (self.data.messages.length > 0)
			{
				var currUserId = null;
				var currGroup = {};
				for(var i = 0; i < self.data.messages.length; i++)
				{
					if (currUserId != self.data.messages[i].user_id)
					{
						if (currUserId != null) self.messages.push(currGroup);
						currUserId = self.data.messages[i].user_id;
						currGroup = {
							name : self.users[currUserId].name,
							profile_image : self.users[currUserId].profile_image,
							type : (currUserId == User.id) ? 'self' : 'other',
							messages : []
						};
					}
					currGroup.messages.push(self.data.messages[i]);
				}
				self.messages.push(currGroup);
			}

			if (self.data.title) self.title = self.data.title;
			else self.title = self.users[self.friendId].name;
		}
		,setReloadChatTimeout : function(){
			$timeout(self.reloadChat, 1000);
		}
		,invokeReloadLoop : function(wait)
		{
			if (self.reloadLoopOn) return; //don't double loop
			if (wait) self.setReloadChatTimeout();
			else self.reloadChat();
		}
		,reloadChat : function()
		{
			if ( ! self.currId)
			{
				self.reloadLoopOn = false;
				return;
			}
			self.reloadLoopOn = true;
			var prevLength = self.data.messages ? self.data.messages.length : 0;
		    $http({
		        method: 'GET',
		        url: Url.api + '/user/' + User.id + '/chat/' + self.currId})
		    .success(function(data){
			    spinnerplugin.hide();
		    	self.data = data.results[0];
				self.crunchData();
				if (self.data.messages.length > prevLength || self.forceScrollDown){
					self.forceScrollDown = false;
					$rootScope.scrollDown();
				} 
				self.setReloadChatTimeout();
			});
		}
	};
	return self;
});