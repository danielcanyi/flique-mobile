module.factory('Fb', function($http, $rootScope, User) {
	var self = {
		loginSuccess : function(userData){},
		loginFailure : function (error) { alert("" + error) },
		login : function(){self.loginSuccess();},
		postSuccess : function(){},
		post : function(description){self.postSuccess();},
	};
	document.addEventListener('deviceready', function() {
		var getMe = function(){
	      	facebookConnectPlugin.api('me', [], 
	      		function(data){
		          	self.loginSuccess(data);
	          	},
	          	function(error)
	          	{
	          		self.loginFailure(error);
	          	}
	          	);
		};
		self.login = function(){
			facebookConnectPlugin.getLoginStatus(function(response){	
				if(response.status !== 'connected')
				{
			      facebookConnectPlugin.login(["basic_info", "email"],
			          function (userData) {
			          	getMe();
			          },
			          function (error) { alert("" + error) }
			      );
				}
				else getMe();
			},
			function (response) { alert(JSON.stringify(response)) });
		};
		self.post = function(description){
			facebookConnectPlugin.showDialog( 
		    {
		        method: "feed",
		        link : 'www.flique.com',
		        name : "Flique",
		    	caption : (User.name ? User.name : User.email) + ' just posted a status on Flique',
		        description: description,    
		    }, 
		    function (response) { self.postSuccess();},
		    function (response) { self.postSuccess();});
		};
	}, false);
	return self;
});