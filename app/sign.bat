call phonegap build android
cd platforms/android
call ant release
cd bin
call jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore /Users/User/Documents/flique-mobile/app/flique_keystore.keystore CordovaApp-release-unsigned.apk flique
call jarsigner -verify -verbose -certs CordovaApp-release-unsigned.apk
rm flique.apk
call zipalign -v 4 CordovaApp-release-unsigned.apk flique.apk
cd ../../../
rm ../../flique/resources/apk/flique.apk
cp platforms/android/bin/flique.apk ../../flique/resources/apk/flique.apk